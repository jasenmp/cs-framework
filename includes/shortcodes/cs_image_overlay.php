<?php
/**
 * Created by PhpStorm.
 * User: jasen
 * Date: 8/10/2016
 * Time: 5:31 PM
 * Description: Image Overlay Shortcode
 */


if (!function_exists('cs_image_overlay')) {

    function cs_image_overlay($atts, $content = '', $key = '')
    {

        extract(shortcode_atts(array(

            'id' => '',
            'class' => '',
            'in_style' => '',

        ), $atts));

        $id = ($id) ? ' id="' . $id . '"' : '';
        $class = ($class) ? ' ' . $class : '';
        $in_style = ($in_style) ? ' style="' . $in_style . '"' : '';

        wp_enqueue_script('cs-image-overlay');

        // begin output

        $output = '';

        $output .= '<div' . $id . ' class="cs-image-overlay' . $class . '">';

        $output .= do_shortcode($content);

        $output .= '</div>';

        return $output;


    }

    add_shortcode('cs_image_overlay', 'cs_image_overlay');
}

/**
 * Created by PhpStorm.
 * User: jasen
 * Date: 8/10/2016
 * Time: 5:31 PM
 * Description: Image Overlay Item Shortcode
 */

if (!function_exists('cs_image_overlay_item')) {
    function cs_image_overlay_item($atts, $content = '', $key = '')
    {

        extract(shortcode_atts(array(
            'background_image' => '',
            'background_color' => '',
            'background_opacity' => '',

        ), $atts));

        ($background_image) ? $background_image_url = 'style="background-image: url(' . $background_image . '); background-size: cover; -webkit-background-size: cover; background-repeat: no-repeat"' : '';

        ($background_color && empty($background_opacity)) ? $background_color = 'style="background-color:' . $background_color . '; opacity:' . $background_opacity . '"' : '';

        ($background_color && $background_opacity) ?$background_color = 'style="background-color:' . $background_color . '; opacity:' . $background_opacity . '"' : '';

            return '<div class="cs-image-overlay-item"'.$background_image_url.'><div class="tint"' . $background_color . '></div><div class="cs-image-overlay-wrap"><div class="cs-image-overlay-wrap-inner">' . do_shortcode($content) . '</div></div></div>';



    }

    add_shortcode('cs_image_overlay_item', 'cs_image_overlay_item');
}
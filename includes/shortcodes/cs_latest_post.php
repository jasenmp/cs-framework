<?php
/**
 * Created by PhpStorm.
 * User: jasen
 * Date: 8/9/2016
 * Time: 6:49 PM
 * Description: CS Latest Post Shortcode
 */

if (!function_exists('cs_latest_post')) {

    function cs_latest_post( $atts, $content = '', $key = '' ) {

        global $wp_query, $post;

        $defaults = array(
            'id' => '',
            'class' => '',
            'cats' => 0,
            'limit' => '',
        );

        extract(shortcode_atts($defaults, $atts));

        // Query

        $args = array(
            'posts_per_page' => $limit,
            'post_type' => 'post',
            'post_status' => 'publish',
        );

        $tmp_query = $wp_query;
        $wp_query = new WP_Query($args);

        ob_start();

        if (have_posts()) :

            while (have_posts()) : the_post();

                // Variables

                $id = get_the_ID();

                $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full-size');

                $title = get_the_title();

                $excerpt = get_the_excerpt();

                // Layout

                $layout = '';

                $layout .= '<section class="cs-latest-post" style="background: url('.( !empty($image) ? $image[0] : '').');">';

                $layout .= $title;

                $layout .= $excerpt;

                $layout .= '</section>';

                echo $layout;

            endwhile;

        endif;

        $output = ob_get_clean();

        wp_reset_query();
        wp_reset_postdata();
        $wp_query = $tmp_query;

        return $output;

    }

    add_shortcode( 'cs_latest_post', 'cs_latest_post' );
    add_shortcode( 'vc_latest_post', 'cs_latest_post' );

}